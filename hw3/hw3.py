# Script for ATS601 Homework 3
# September 1, 2017

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset

# ------ Import ERA-Interim Data ------
# Data is 6-hourly on the 300 hPa pressure surface for January 1-8, 2014

#fpath = '/Users/andrea/Documents/School/grad/TA_ATS601/ATS601-Homework/hw3/'
fpath = '/Users/andrea/Documents/Class/Fall17/ATS601_TA/homework/hw3/'
fname = 'homework_3_ERA-interim_300hPa_2014-Jan1-8.nc'
data  = Dataset(fpath+fname,'r')

# Read variables and close data file. netCDF4 package automatically applies
# scales and offsets
lon = data.variables['longitude'][:]
lat = data.variables['latitude'][:]
u   = data.variables['u'][:]
v   = data.variables['v'][:]
data.close()

# ------ Problem 2: Approximations to the momentum equations ------
# (a) Estimate the typical values* of the local accelerations of the zonal and
# meridional wind in the Northern midlatitudes (30-60N), namely: du/dt, dv/dt
# To estimate these time tendencies, use the centered finite difference method
#   du   u(i+1) - u(i-1)
#   -- = ---------------
#   dt       2 * dt
# * One can calculate a typical magnitude in many different ways. You could
# provide the mean of the absolute value, provide the most common magnitude,
# etc. One nice way is to calculate the RMS value. 
# ------
# Centered finite difference function
def centdiff(wind,dt):
    ddt = np.zeros(np.shape(wind))
    ddt[:] = np.nan
    for itime in range(np.shape(wind)[0]-1):
        ddt[itime+1,:,:] = (wind[itime + 1,:,:] - wind[itime - 1,:,:]) \
            / (2 * dt)
    return ddt

# Cosine latitude weight mean function
def cosw_mean(x,lats):
    w  = np.cos(lats)
    w3d = np.tile(np.transpose(np.tile(w,(np.shape(x)[2], 1))),\
        (np.shape(x)[0],1,1))

    # Average over all latitudes
    mx = np.nansum(x * w3d, axis = 1) / np.nansum(w3d, axis = 1)
    # Average over remaining dimensions
    mx1d = np.nanmean(mx)
    return mx1d
    
# ------
# Define constants
latmin = 30
latmax = 60
dt     = 6. * 60 * 60 # 6 hours * 60 min * 60 seconds

# Find latitude indices for 30-60N. Latitudes are ordered 90 to -90 so ilat1 is
# the higher latitude, ilat2 is the lower latitude
ilat2 = np.argmin(np.abs(lat - latmin))
ilat1 = np.argmin(np.abs(lat - latmax))

# Calculate the local acceleration of the wind at ALL gridpoints
dudt = centdiff(u,dt)
dvdt = centdiff(v,dt)

# Estimate the typical magnitude in the Northern latitudes. Area weight by cos-
# latitude
# (1) Mean of absolute value
m_dudt = cosw_mean(np.abs(dudt[:,ilat1:ilat2,:]),lat[ilat1:ilat2])
m_dvdt = cosw_mean(np.abs(dvdt[:,ilat1:ilat2,:]),lat[ilat1:ilat2])
# (2) Root mean square value
rms_dudt = np.sqrt(cosw_mean(np.square(dudt[:,ilat1:ilat2,:]),lat[ilat1:ilat2]))
rms_dvdt = np.sqrt(cosw_mean(np.square(dvdt[:,ilat1:ilat2,:]),lat[ilat1:ilat2]))

# ---- Uncomment the following to see answers with no area weighting ----
## (1) Mean of absolute value
#m_dudt = np.nanmean(np.abs(dudt[:,ilat1:ilat2,:]))
#m_dvdt = np.nanmean(np.abs(dvdt[:,ilat1:ilat2,:]))
## (2) Root mean square value
#rms_dudt = np.sqrt(np.nanmean(np.square(dudt[:,ilat1:ilat2,:])))
#rms_dvdt = np.sqrt(np.nanmean(np.square(dvdt[:,ilat1:ilat2,:])))

print('Mean u acceleration ' + str(m_dudt) + ' m/s^2')
print('Mean v acceleration ' + str(m_dvdt) + ' m/s^2')
print('RMS u acceleration ' + str(rms_dudt) + ' m/s^2')
print('RMS v acceleration ' + str(rms_dvdt) + ' m/s^2')