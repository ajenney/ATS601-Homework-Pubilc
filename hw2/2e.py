# Script to create figure from ATS601 Homework 2, problem 2e
# August 28, 2017

import numpy as np
import matplotlib.pyplot as plt

# ------ Define constants
lam   = 0.10
dt    = 0.01
dx    = 0.10
mu    = 5.0
sigma = 1.0
xmax  = 70

# ------ Initiliaze x-grid
#x = np.arange(0,xmax+dx,dx)
x = np.arange(-5, xmax+dx, dx)
plot_times = np.arange(0,24,2)

# ------ Initial Gaussian pulse
den2plot = np.zeros((len(plot_times),len(x)))
den0 = np.exp(-(x-mu)**2/(2*sigma**2))
den2plot[0,:] = den0

# ================================================================
# Loop through times to compute density at each timestep
counter = 1
for t in np.arange(0,22+dt,dt):
    # Loop through spacial points. Initialize first point because the loop uses
    # data from "previous" points
    den1 = np.zeros((len(x)))
    for ix in range(1,len(x)):
        term1 = den0[ix]
        term2 = dt * lam * np.absolute(x[ix]) * (den0[ix] - den0[ix-1]) / dx
        term3 = lam * dt * den0[ix]
        if ix >= 0:
            den1[ix] = term1 - term2 - term3
        if ix < 0:
            den1[ix] = term1 - term2 + term3
            
    den0 = den1
    
    # Save density structure if timestep is one of the ones to plot
    if any(plot_times - t == 0) and t != 0:
        #print(t)
        den2plot[counter,:] = den0
        counter = counter + 1

# ================================================================
# Plot Figure

plt.figure()
plt.ylabel('Density', fontsize=20)
plt.xlabel('x', fontsize=20)
plt.title('Homework 2 Solution: Evolution of Psi')
plt.xlim((-5, xmax))
colormap = plt.cm.ocean
plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 0.9, len(den2plot))])

for iplot in range(len(plot_times)):
    plt.plot(x,den2plot[iplot,:],linewidth=1.5)
    
plt.show()

