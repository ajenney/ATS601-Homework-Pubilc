# ATS601 Homework 8, Problem 1d
# November 14, 2017

import matplotlib.pyplot as plt
import cmath as cm
import numpy as np

# --- User-defined variables ---
t = np.arange(0,4*np.pi, np.pi/24)          # Time
N2 = [1, -1, 0]                             # Three cases for N^2

# --- Buoyancy frequency ---
N  = N2                                     # Initiliaze N
for i in range(3): N[i]  = cm.sqrt(N2[i])   # Use cmath for imaginary #s

# --- Compute time series of zprime ---
#zprime = np.zeros((3,len(t)))
zprime = np.empty([3,len(t)])
for icase in range(3):
    for itime in range(len(t)):
        zprime[icase,itime] = cm.exp(1j*(-1.0*N[icase]*t[itime])).real

# ====================================================================
# --- Create figure of vertical position ---
plt.figure(figsize=(5,5))

for icase in range(3):
    plt.plot(t,zprime[icase,:])
    
plt.plot([-1, 13],[0, 0], linewidth=0.5, color='tab:grey',ls='--')

plt.xlim([-1, 13])
plt.ylim([-2, 10])    
plt.xlabel('Time (s)')
plt.ylabel('z\' (m)')
plt.title('Evolution of z\' with varying cases of $N^2$')
plt.legend(('$N^2$ > 0','$N^2$ < 0','$N^2$ = 0'))
plt.show()
