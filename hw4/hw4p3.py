# ATS601 Homework 4, Problem 3
# September 18, 2017

import numpy as np
import matplotlib.pyplot as plt

# ====== How good are the f- and Beta- plane approximations? ======

# ------ Define constants ------
lat1 = np.radians(5)
lat2 = np.radians(45)
omega = 7.292 * 10**(-5)
latsd = np.arange(-90,90,0.2)
lats = np.radians(latsd)

# ------ Calculate error ------

# baseline Coriolis for comparison 
baseline_cor = 2 * omega * np.sin(lats)

# f- and Beta- approximations for all latitudes -
f1 = 2 * omega * np.sin(lat1)
f2 = 2 * omega * np.sin(lat2)

b1 = 2 * omega * (np.sin(lat1) + (np.cos(lat1)*(lats- lat1)))
b2 = 2 * omega * (np.sin(lat2) + (np.cos(lat2)*(lats- lat2)))

# subtract approximation from basline
f1_err = 100 * abs((baseline_cor - f1)/baseline_cor)
f2_err = 100 * abs((baseline_cor - f2)/baseline_cor)

b1_err = 100 * abs((baseline_cor - b1)/baseline_cor)
b2_err = 100 * abs((baseline_cor - b2)/baseline_cor)

# ========================= Plot =========================
import matplotlib

fig = plt.figure(figsize=(12,10))

ax1 = fig.add_subplot(221)
ax1.plot(latsd, f1_err,'k')
ax1.plot((2, 8), (10, 10), '--k')
plt.xlim(2,8)
plt.ylim(0, 250)
plt.ylabel('% Error')
plt.xlabel('Latitude')
plt.title('3(a): f-plane approx error about 5N')

ax2 = fig.add_subplot(222)
ax2.plot(latsd, f2_err,'k')
ax2.plot((0, 90), (10, 10), '--k')
plt.xlim(0,90)
plt.ylim(10**(-2), 10**5)
ax2.set_yscale('log')
plt.ylabel('% Error')
plt.xlabel('Latitude')
plt.title('3(b): f-plane approx error about 45N')

ax3 = fig.add_subplot(223)
ax3.plot(latsd, b1_err,'k')
ax3.plot((0, 45), (10, 10), '--k')
plt.xlim(0, 45)
plt.ylim(0, 30)
plt.ylabel('% Error')
plt.xlabel('Latitude')
plt.title('3(c): Beta-plane approx error about 5N')

ax4 = fig.add_subplot(224)
ax4.plot(latsd, b2_err,'k')
ax4.plot((0, 90), (10, 10), '--k')
plt.xlim(0,90)
plt.ylim(10**(-2), 10**5)
ax4.set_yscale('log')
plt.ylabel('% Error')
plt.xlabel('Latitude')
plt.title('3(d): Beta-plane approx error about 45N')

plt.tight_layout()
fig = plt.gcf()

plt.show()