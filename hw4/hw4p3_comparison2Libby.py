# ATS601 Homework 4, Problem 3
# September 18, 2017
# Comparison to Libby's answer

import numpy as np
import matplotlib.pyplot as plt

# ====== How good are the f- and Beta- plane approximations? ======

# ------ Define constants ------
lat1 = np.radians(5)
lat2 = np.radians(45)
omega = 7.292 * 10**(-5)
latsd = np.arange(-90,90,0.2)
lats = np.radians(latsd)

# ------ Calculate error ------

# baseline Coriolis for comparison 
baseline_cor = 2 * omega * np.sin(lats)

# f- and Beta- approximations for all latitudes -
f1 = 2 * omega * np.sin(lat1)
f2 = 2 * omega * np.sin(lat2)

b1 = 2 * omega * (np.sin(lat1) + (np.cos(lat1)*(lats- lat1)))
b2 = 2 * omega * (np.sin(lat2) + (np.cos(lat2)*(lats- lat2)))

# subtract approximation from basline
f1_err = -100 * ((baseline_cor - f1)/baseline_cor)
f2_err = -100 * ((baseline_cor - f2)/baseline_cor)

b1_err = -100 * ((baseline_cor - b1)/baseline_cor)
b2_err = -100 * ((baseline_cor - b2)/baseline_cor)

# ========================= Plot =========================
import matplotlib

fig = plt.figure(figsize=(12,10))

ax1 = fig.add_subplot(221)
ax1.plot(f1_err,latsd,'k')
#ax1.plot((2, 8), (10, 10), '--k')
plt.ylim(0,90)
plt.xlim(-40, 40)
plt.xlabel('% Error')
plt.ylabel('Latitude')
plt.title('3(a): f-plane approx error about 5N')

ax2 = fig.add_subplot(222)
ax2.plot(f2_err,latsd,'k')
#ax2.plot((0, 90), (10, 10), '--k')
plt.ylim(0,90)
plt.xlim(-40, 40)
#ax2.set_yscale('log')
plt.xlabel('% Error')
plt.ylabel('Latitude')
plt.title('3(b): f-plane approx error about 45N')

ax3 = fig.add_subplot(223)
ax3.plot(b1_err,latsd,'k')
#ax3.plot((0, 45), (10, 10), '--k')
plt.ylim(0,90)
plt.xlim(-40, 40)
plt.xlabel('% Error')
plt.ylabel('Latitude')
plt.title('3(c): Beta-plane approx error about 5N')

ax4 = fig.add_subplot(224)
ax4.plot(b2_err,latsd,'k')
#ax4.plot((0, 90), (10, 10), '--k')
plt.ylim(0,90)
plt.xlim(-40, 40)
#ax4.set_yscale('log')
plt.xlabel('% Error')
plt.ylabel('Latitude')
plt.title('3(d): Beta-plane approx error about 45N')

plt.tight_layout()
fig = plt.gcf()

plt.show()