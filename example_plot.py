import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt

x = np.zeros((9,9))
x[4,4] = 10.0

# ------ Set plotting constants -------
cmap = cm.get_cmap('terrain',5)
clevs = range(11)
fs = 20

# ------ Plot -------
f, (a0, a1) = plt.subplots(1,2, figsize = (10,4), \
    gridspec_kw = {'width_ratios':[1, 1]})
im = a0.contourf(x, clevs, cmap=cmap)
a0.set_title('Contour fill')
im = a1.imshow(x, cmap=cmap)
a1.set_title('Raster fill')
a1.set_xlim(0,8)
a1.set_ylim(0,8)

f.subplots_adjust(right=0.85)
f.colorbar(im, f.add_axes([0.9, 0.15, 0.05, 0.7]))
f.show()
