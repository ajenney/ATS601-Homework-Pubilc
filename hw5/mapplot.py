# Contour map P=plotting function using Basemap package

def cmapplot(lon, lat, data, clevs = None, nlevs = 11.0, cfill = True, \
    coastlines = True, countries = False, fill_land = False, proj = 'cyl', \
    politicalboundaryLW = 1.0, centerlat = 0.0, centerlon = 0.0, \
    cmap = 'viridis', zeromid = False):

    from mpl_toolkits.basemap import Basemap
    import numpy as np
    import matplotlib.cm as cm
    
    # Mask non-finite data
    #data[np.where(~np.isfinite(data))] = np.nan
    
    # Lat-lon range
    latcorners = (np.min(lat), np.max(lat))
    loncorners = (np.min(lon), np.max(lon))
    
    # Create map
    m = Basemap(projection = proj, \
        llcrnrlat=latcorners[0],urcrnrlat=latcorners[1],\
        llcrnrlon=loncorners[0],urcrnrlon=loncorners[1], \
        resolution='c')

    # Draw coastlines and countries
    if coastlines: m.drawcoastlines(linewidth = politicalboundaryLW)
    if countries: m.drawcountries(linewidth = politicalboundaryLW)
    if fill_land: m.fillcontinents(color='grey')
    
    # Get nice contour levels if none are defined
    if clevs is None:
        # Get order of magnitude of data maximum
        cmax = np.ceil(np.nanmax(data))
        cmin = np.floor(np.nanmin(data))
        order = np.floor(np.log10(cmax))
        cmax = np.ceil(cmax/(10.**order)) * 10.**order
        cmin = np.floor(cmin/(10.**order)) * 10.**order
        
        # Center contour levels on zero if zeromid specified
        if zeromid:
            cmax = np.nanmax((abs(cmax), abs(cmin)))
            cmin = -1 * cmax
        if (cmax - cmin) / 10.**order > nlevs: 
            cstep = 2*(10.**order)
            if np.mod(cmax,cstep) != 0:
                cmax = cmax + 10.**order
            if np.mod(cmin,cstep) != 0:
                cmin = cmin - 10.**order
        else: cstep = 10.**order
        clevs = np.arange(cmin, cmax+cstep, cstep)
    
    # Convert latitude and longitude values to plot x/y values
    x,y = m(*np.meshgrid(lon, lat))
    
    # Draw contours (filled or not filled)
    if cfill: cs = m.contourf(x, y, data, clevs, cmap=cm.get_cmap(cmap) ,extend="both")
    else: cs = m.contour(x, y, data, clevs, lindwidths = 1.5, cmap=cm.get_cmap(cmap))
    
    return cs