# Script for ATS601 Homework 5 Problem 2
# September 25, 2017

# +++++++++++++++++++++++++++++++ R E A D   M E ++++++++++++++++++++++++++++++++
# Part (a) - single time step
# This is the default for the script. See below to change script to make plots
# for parts (c) and (d)

# Part (c) - average over all time steps
# Part (d) - zonally average and plot as a function of latitude
partcd = True # Set this to True for parts (c) and (d)
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import numpy as np
from netCDF4 import Dataset
import mapplot as mp
import matplotlib.pyplot as plt

# ------ Define constants ------
omega = 7.292 * 10**(-5)
ntimes = 32
smoothing_window = 7.0

if partcd: 
    tstep = range(ntimes)
    titstr0 = '(c) Rossby numbers and zonal wind using 8-day averages'
    titstr1 = '(d) Zonally-averaged Rossby numbers using 8-day averages'
else: 
    tstep = 5
    titstr0 = '(a) Rossby numbers and zonal wind using single timestep'

# ------ Import ERA-Interim Data ------
# Data is 6-hourly on the 300 hPa pressure surface for January 1-8, 2014

fpath  = '/Users/andrea/Documents/Class/Fall17/ATS601_TA/homework/hw5/'
fname  = 'homework_3_ERA-interim_300hPa_2014-Jan1-8.nc'
fname2 = 'homework_5_ERA-interim_uvtnds-300hPa_2014-Jan1-8.nc'
data   = Dataset(fpath + fname,'r')

# Read variables and close first data file
lon = data.variables['longitude'][:]
lat = data.variables['latitude'][:]
u   = data.variables['u'][tstep,:,:]
v   = data.variables['v'][tstep,:,:]
data.close()

# Read variables from second data file
data  = Dataset(fpath + fname2, 'r')
uacc  = data.variables['lutnd'][tstep,:,:] # Zonal wind acceleration
uadvu = data.variables['uadvu'][tstep,:,:] # Zonal advection of zonal wind
vadvu = data.variables['vadvu'][tstep,:,:] # Meridional advection of zonal wind
wadvu = data.variables['wadvu'][tstep,:,:] # Vertical advection of zonal wind
data.close()

# ==============================================================================
# Average over all time steps for parts (c) and (d)
if partcd:
    u = np.nanmean(u,axis=0)
    v = np.nanmean(v,axis=0)
    uacc = np.nanmean(uacc,axis=0)
    uadvu = np.nanmean(uadvu,axis=0)
    vadvu = np.nanmean(vadvu,axis=0)
    wadvu = np.nanmean(wadvu,axis=0)

# ==============================================================================
# Compute required Rossby numbers (only for partcd = True)

# Coriolis acceleration (in the zonal direction = fv)
lat2d = np.radians(np.transpose(np.tile(lat,(np.size(lon), 1))))
cor = 2 * omega * np.sin(lat2d) * v

# (i) Local acceleration of u and the Coriolis acceleration
Ro1 = abs(uacc / cor)

# (ii) Horizontal advection of u and the Coriolis acceleration
Ro2 = abs(np.sqrt(uadvu**2 + vadvu**2) / cor)

# (iii) Vertical advection of u and the Coriolis acceleration
Ro3 = abs(wadvu / cor)

# ==============================================================================
# Zonally-averaged Rossby numbers using 8-day averages (only for partcd = True)
# i.e., average over all longtidues
# ----- Moving average function-----
def movAvg(x, ws = 3.0):
    # Computes unweighted centered moving average of 'x' with window size 'ws'
    if np.mod(ws,2) == 0:
        print('Error: Window size must be an odd number')
        return

    h_ws = int(np.floor(ws/2)); # Half of the window size
    n = np.size(x)
    
    ma = np.zeros(n)
    for ix in range(n):
        if ix < h_ws or ix > n - h_ws - 1:
            ma[ix] = x[ix]
        else:
            ma[ix] = np.sum(np.ma.masked_invalid(x[ix - h_ws : \
                ix + h_ws + 1])) / ws
    return ma
# ----------------------------------
if partcd:
    Ro1_z = movAvg(np.nanmean(Ro1, axis = 1), ws = smoothing_window)
    Ro2_z = movAvg(np.nanmean(Ro2, axis = 1), ws = smoothing_window)
    Ro3_z = movAvg(np.nanmean(Ro3, axis = 1), ws = smoothing_window)

# ==============================================================================
# Plot (4 plots: Ro1-3 and zonal wind field). If partcd = True, then create an
# additional figure showing zonally-averaged Rossby numbers

# Contour levels for plotting
clevs = np.arange(0,1.1,0.1)
colmap = 'Blues'

# Subplot titles
stit = ['(i) Zonal acceleration of u', '(ii) Horiz. advection of u', \
    '(iii) Vert. advection of u']

fig = plt.figure(figsize=(13,7))

ax1 = fig.add_subplot(221)
mp.cmapplot(lon, lat, Ro1, clevs = clevs, cmap = colmap)
cbar = plt.colorbar()
plt.title(stit[0])

ax2 = fig.add_subplot(222)
mp.cmapplot(lon, lat, Ro2, clevs = clevs, cmap = colmap)
cbar = plt.colorbar()
cbar.set_label('Rossby Number')
plt.title(stit[1])

ax3 = fig.add_subplot(223)
mp.cmapplot(lon, lat, Ro3, clevs = clevs, cmap = colmap)
cbar = plt.colorbar()
plt.title(stit[2])

ax4 = fig.add_subplot(224)
mp.cmapplot(lon, lat, u, zeromid = True, cmap = 'PuOr')
cbar = plt.colorbar()
cbar.set_label('Speed (m/s)')
plt.title('Zonal wind')

# Global title for panel plots
fig.suptitle(titstr0, fontsize=14)
fig.tight_layout(pad=3.0) # Add padding to make room for global title
plt.show()

# - - - - - - - - - -
if partcd:
    plt.figure(figsize=(6,8))
    plt.plot(np.transpose((Ro1_z, Ro2_z, Ro3_z)), lat)
    plt.xlabel('Rossby Number')
    plt.ylabel('Latitude (deg-N)')
    plt.ylim(-90,90)
    plt.legend(stit)
    plt.title(titstr1)
    plt.show()

